from sqlalchemy import Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from database import Base


UserAddress = Table(
    'UserAddress',
    Base.metadata,
    Column('id', Integer, primary_key=True),
    Column('userId', Integer, ForeignKey('user.id')),
    Column('addressId', Integer, ForeignKey('address.id'))
)


class User(Base):

    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    surname = Column(String)
    phone = Column(Integer)
    addresses = relationship('Address', secondary=UserAddress, backref='User')


class Address(Base):

    __tablename__ = "address"

    id = Column(Integer, primary_key=True, index=True)
    city = Column(String)
    street = Column(String)
    building = Column(Integer)
    apartment = Column(Integer)
    users = relationship('User', secondary=UserAddress, backref='Address', overlaps="User,addresses")
